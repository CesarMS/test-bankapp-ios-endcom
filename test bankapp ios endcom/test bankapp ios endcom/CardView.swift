//
//  CardView.swift
//  test bankapp ios endcom
//
//  Created by César MS on 01/07/21.
//

import UIKit

class CardView: UIView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageCard: UIImageView!
    @IBOutlet weak var statusCard: UILabel!
    @IBOutlet weak var numCard: UILabel!
    @IBOutlet weak var nameUser: UILabel!
    @IBOutlet weak var saldoCard: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("CardView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask =  [.flexibleHeight, .flexibleWidth]
        
    }
}
