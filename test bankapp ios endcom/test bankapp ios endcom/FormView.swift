//
//  FormView.swift
//  test bankapp ios endcom
//
//  Created by César MS on 01/07/21.
//

import Foundation
import UIKit
class FormView: UIViewController {
    
    @IBOutlet weak var numTarjeta: UITextField!
    @IBOutlet weak var cuenta: UITextField!
    @IBOutlet weak var Issure: UITextField!
    @IBOutlet weak var nombreTarjeta: UITextField!
    @IBOutlet weak var marca: UITextField!
    @IBOutlet weak var estatus: UITextField!
    @IBOutlet weak var saldo: UITextField!
    @IBOutlet weak var tipocuenta: UITextField!
    
    
    
    
    @IBAction func AddCard(_ sender: Any) {
        
        let parameters = [
            "Numero": numTarjeta.text!,
            "Cuenta": cuenta.text!,
            "Issure": Issure.text!,
            "Nombre": nombreTarjeta.text!,
            "Marca": marca.text!,
            "Estatus": estatus.text!,
            "Saldo": saldo.text!,
            "Tipo": tipocuenta.text!
        ] as [String : Any]
        
        let alert = UIAlertController(title: "Agregar tarjeta", message: "Deseas agregar esta información \(parameters)", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Continue", style: UIAlertAction.Style.default, handler: nil))
                        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func Cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
