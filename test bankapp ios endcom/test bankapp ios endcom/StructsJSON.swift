//
//  StructsJSON.swift
//  test bankapp ios endcom
//
//  Created by César MS on 01/07/21.
//

import Foundation

struct Cuentas: Codable {
    let cuenta: [Cuenta]
}
struct Cuenta: Codable {
    let cuenta: Int
    let nombre, ultimaSesion: String
    let id: Int
}



struct Saldoos: Codable {
    let saldos: [Saldo]
}

struct Saldo: Codable {
    let cuenta, saldoGeneral, ingresos, gastos: Int
    let id: Int
}


struct Tarjetas: Codable {
    let tarjetas: [Tarjeta]
}

// MARK: - Tarjeta
struct Tarjeta: Codable {
    let tarjeta, nombre: String
    let saldo: Int
    let estado, tipo: String
    let id: Int
}

struct Movimientos: Codable {
    let movimientos: [Movimiento]
}

// MARK: - Movimiento
struct Movimiento: Codable {
    let fecha, descripcion, monto: String
    let tipo: Tipo
    let id: Int
}

enum Tipo: String, Codable {
    case abono = "abono"
    case cargo = "cargo"
}
