//
//  ViewController.swift
//  test bankapp ios endcom
//
//  Created by César MS on 01/07/21.
//

import UIKit

class Saldos: UICollectionViewCell {
    
    @IBOutlet weak var textSaldo: UILabel!
    @IBOutlet weak var numSaldo: UILabel!
    
}


class cellMovs: UITableViewCell {
    
    @IBOutlet weak var nameMov: UILabel!
    @IBOutlet weak var dateMov: UILabel!
    @IBOutlet weak var montoMov: UILabel!
    
}

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var encabezado: ViewUser!
    @IBOutlet weak var buttonCuentas: UIButton!
    @IBOutlet weak var buttonSend: UIButton!
    @IBOutlet weak var collectionSaldos: UICollectionView!
    @IBOutlet weak var card1: CardView!
    @IBOutlet weak var card2: CardView!
    @IBOutlet weak var tableMovs: UITableView!
    
    
    var textSaldos = ["Saldo general en cuentas", "Total de ingresos", "Total de gastos"]
    var arraySaldos = [String]()
    
    
    var textMovs = [String]()
    var saldoMovs = [String]()
    var dateMovs = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonCuentas.underline()
        buttonSend.underline()
        
        
        self.tableMovs.delegate = self
        self.tableMovs.dataSource = self
        services()

        
    }
    
    func services(){
        // Cuentas
        guard let urlCuenta = URL(string: "http://bankapp.endcom.mx/api/bankappTest/cuenta") else {
            return
        }
        URLSession.shared.dataTask(with: urlCuenta) { (data, respone, error) in
            guard let data = data else { return }
            do {
                let cuentas = try JSONDecoder().decode(Cuentas.self, from: data)
                DispatchQueue.main.async {
                    self.encabezado.userName.text = cuentas.cuenta[0].nombre
                    self.encabezado.dateSession.text = cuentas.cuenta[0].ultimaSesion
                }
            } catch{
                print("ERROR", error.localizedDescription)
            }
        }.resume()
        // Saldos
        guard let urlSaldos = URL(string: "http://bankapp.endcom.mx/api/bankappTest/saldos") else {
            return
        }
        URLSession.shared.dataTask(with: urlSaldos) { (data, respone, error) in
            guard let data = data else { return }
            do {
                let saldos = try JSONDecoder().decode(Saldoos.self, from: data)
                DispatchQueue.main.async {
                    self.arraySaldos.append("$" +  String(saldos.saldos[0].saldoGeneral) + ".00")
                    self.arraySaldos.append("$" +  String(saldos.saldos[0].ingresos) + ".00")
                    self.arraySaldos.append("$" +  String(saldos.saldos[0].gastos) + ".00")
                    self.collectionSaldos.dataSource = self
                }
            } catch{
                print("ERROR", error.localizedDescription)
            }
        }.resume()
        // Tarjetas
        guard let urlTarjetas = URL(string: "http://bankapp.endcom.mx/api/bankappTest/tarjetas") else {
            return
        }
        URLSession.shared.dataTask(with: urlTarjetas) { (data, respone, error) in
            guard let data = data else { return }
            do {
                let tarjetas = try JSONDecoder().decode(Tarjetas.self, from: data)
                DispatchQueue.main.async {
                    
                    self.card1.statusCard.text = tarjetas.tarjetas[0].estado
                    self.card1.numCard.text = tarjetas.tarjetas[0].tarjeta
                    self.card1.nameUser.text = tarjetas.tarjetas[0].nombre
                    self.card1.saldoCard.text = "$" + String(tarjetas.tarjetas[0].saldo) + ".00"
                    
                    self.card2.statusCard.text = tarjetas.tarjetas[1].estado
                    self.card2.numCard.text = tarjetas.tarjetas[1].tarjeta
                    self.card2.nameUser.text = tarjetas.tarjetas[1].nombre
                    self.card2.saldoCard.text = "$" + String(tarjetas.tarjetas[1].saldo) + ".00"
                }
            } catch{
                print("ERROR", error.localizedDescription)
            }
        }.resume()
        // Movimientos
        guard let urlMovimientos = URL(string: "http://bankapp.endcom.mx/api/bankappTest/movimientos") else {
            return
        }
        URLSession.shared.dataTask(with: urlMovimientos) { (data, respone, error) in
            guard let data = data else { return }
            do {
                let movimientos = try JSONDecoder().decode(Movimientos.self, from: data)
                DispatchQueue.main.async {
                    for i in 0...(movimientos.movimientos.count - 1) {
                        self.dateMovs.append(movimientos.movimientos[i].fecha)
                        self.saldoMovs.append(movimientos.movimientos[i].monto)
                        self.textMovs.append(movimientos.movimientos[i].tipo.rawValue)
                        self.tableMovs.reloadData()
                    }
                }
            } catch{
                print("ERROR", error.localizedDescription)
            }
        }.resume()
        
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return textSaldos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellSaldos", for: indexPath) as! Saldos
        
        cell.textSaldo.text = textSaldos[indexPath.item]
        cell.numSaldo.text = arraySaldos[indexPath.item]
        
        return cell
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return textMovs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "movsCell", for: indexPath) as! cellMovs
        let row = indexPath.row
        
        cell.nameMov.text = textMovs[row]
        cell.dateMov.text = dateMovs[row]
        cell.montoMov.text = saldoMovs[row]
        
        return cell
    }
    
}
